const {
  src,
  dest,
  series
} = require('gulp');



const beautify = require('gulp-beautify');
const del = require('del');
const concat = require('gulp-concat');
const imagemin = require("gulp-imagemin");
const newer = require("gulp-newer");
const ttf2woff = require("gulp-ttf2woff");
const ttf2woff2 = require("gulp-ttf2woff2");
const merge = require('merge-stream');
const include = require('gulp-file-include');;
const cleanCSS = require('gulp-clean-css');
const sass = require('gulp-sass')(require('sass'));
const sourcemaps = require('gulp-sourcemaps');


const appFolder = "app/"; //work folder name
const distFolder = "dist/"; //project folder name

const appFolderWP = "app/"; //work folder name
const distFolderWP = "../"; //project folder name


const foldersCopy = ['js', 'parts'];
const foldersCopy2 = ['images'];






function html() {
  return src(appFolder+'**.+(php|html)')
      .pipe(include({
          prefix: '@@'
      }))
      .pipe(beautify.html({
          indent_size: 4
      }))
      .pipe(dest((distFolder)))
}

function images() {
  return src(appFolder + "/images/**/*.+(png|jpg|gif|ico|svg|webp)")
    .pipe(newer(distFolder+'images'))
    .pipe(
      imagemin({
        progressive: true,
        svgPlugins: [
          {
            removeViewBox: false,
          },
        ],
        interlaced: true,
        optimizationLevel: 3, //0 to 7,
      })
    )
    .pipe(dest(distFolder+'images'))
}

function fonts() {
  return (
    src(appFolder+'fonts/*')
      .pipe(ttf2woff())
      .pipe(dest(distFolder+'fonts'))
      .pipe(ttf2woff2())
      .pipe(dest(distFolder+'fonts'))
  );
}

function css() {
  return src(appFolder+'scss/all.scss')
      .pipe(sass())
      .pipe(concat('main.min.css'))
      .pipe(sourcemaps.init())
      .pipe(cleanCSS({compatibility: 'ie8'}))
      .pipe(sourcemaps.write())
      .pipe(dest(distFolder+'css'))
}

function copy() {
  let tasks = foldersCopy.map(function (folder) {
      return src(appFolder + folder + '/**')
          .pipe(dest(distFolder + folder));
  });
  return merge(tasks);
}



function Moveimages() {
let tasks = foldersCopy2.map(function (folder) {
    return src(appFolder + folder + '/**')
        .pipe(dest(distFolder + folder));
});
return merge(tasks);
}




function clear() {
  return del(distFolder)
}




function clearWP(){
return del(distFolderWP+'css', {force: true})
}


function cssWP(){
return src(appFolderWP+'scss/all.scss')
      .pipe(sass())
      .pipe(concat('main.min.css'))
      .pipe(cleanCSS({compatibility: 'ie8'}))
      .pipe(dest(distFolderWP+'css'))
}

exports.build = series(clear, html, images, fonts, css, copy);
exports.wp = series(clearWP, cssWP);


