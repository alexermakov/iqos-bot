export const QUESTIONS = [{
        name: 'Bronze Selection',
        title: 'Насыщенный <br>и тёплый',
        help: 'У этого вкуса ароматические ноты «Какао»',
        image: '1',
        image2: '1',
        text: 'Мягкий ароматный табачный бленд с лёгкими нотами какао и сухофруктов.',
        variants: [
            ['Конечно, это HEETS BRONZE SELECTION', 1],
            ['Возможно, это HEETS RUBY FUSE', 0],
            ['Я знаю, это HEETS CREATIONS APRICITY', 0],
        ],
    },

    {
        name: 'Amber Selection',
        title: 'Сбалансированный <br>и пряный',
        help: 'У этого вкуса ароматические ноты «Ореховые»',
        image: '2',
        image2: '3',
        text: 'Табак особой обжарки с лёгкими древесными и ореховыми нотками.',
        variants: [
            ['Я знаю, это HEETS GREEN ZING',0],
            ['Возможно, это HEETS GOLD SELECTION', 0],
            ['Конечно, это HEETS AMBER SELECTION', 1],
        ],
    },

    {
        name: 'Slate Selection',
        title: 'Мягкий <br>и утончённый',
        help: 'У этого вкуса ароматические ноты «Лёгкий цитрус»',
        image: '3',
        image2: '4',
        text: 'Мягкий и ароматный табачный бленд с деликатными пряными нотами.',
        variants: [
            ['Возможно, это HEETS TURQUOISE SELECTION', 0],
            ['Конечно, это HEETS SLATE SELECTION', 1],
            ['Я знаю, это HEETS SUMMER BREEZE', 0],
        ],
    },

    {
        name: 'Gold Selection',
        title: 'Сбалансированный <br>и свежий',
        help: 'У этого вкуса ароматические ноты «Лёгкая свежесть»',
        image: '4',
        image2: '5',
        text: 'Бархатный вкус табака особой обжарки с древесными нотами и лёгким освежающим оттенком.',
        variants: [
            ['Возможно, это HEETS SATIN FUSE', 0],
            ['Я знаю, это HEETS AMBER SELECTION', 0],
            ['Конечно, это HEETS GOLD SELECTION', 1],
        ],
    },

    {
        name: 'Ruby Fuse',
        title: 'Насыщенный ягодный',
        help: 'У этого вкуса ароматические ноты «Красные ягоды»',
        image: '5',
        image2: '6',
        text: 'Мягкий и ароматный табачный бленд дополнен выразительными ягодными нотами и с лёгким цветочным оттенком.',
        variants: [
            ['Я знаю, это HEETS PURPLE WAVE', 0],
            ['Возможно, это HEETS CREATIONS YUGEN', 0],
            ['Конечно, это HEETS RUBY FUSE', 1],
        ],
    },

    {
        name: 'Amarelo Fuse',
        title: 'Насыщенный <br>и цитрусовый',
        help: 'У этого вкуса ароматические ноты «Цитрусовые»',
        image: '6',
        image2: '7',
        text: 'Мягкий и ароматный табачный бленд с выразительными цитрусовымии деликатными пряными нотами.',
        variants: [
            ['Возможно, это HEETS GREEN ZING', 0],
            ['Я знаю, это HEETS SUMMER BREEZE', 0],
            ['Конечно, это HEETS AMARELO FUSE', 1],
        ],
    },



    {
        name: 'Satin Fuse',
        title: 'Насыщенный <br>и экзотический',
        help: 'У этого вкуса ароматические ноты «Тропические фруктовые»',
        image: '7',
        image2: '8',
        text: 'Мягкий и ароматный табачный бленд дополнен выразительными нотами тропических фруктов.',
        variants: [
            ['Я знаю, это HEETS GOLD SELECTION', 0],
            ['Возможно, это HEETS AMBER SELECTION', 0],
            ['Конечно, это HEETS SATIN FUSE', 1],
        ],
    },

    {
        name: 'Turquoise Selection',
        title: 'Освежающий <br>и охлаждающий',
        help: 'У этого вкуса ароматические ноты «Свежие»',
        image: '8',
        image2: '9',
        text: 'Холод ментола и бархатистый табачный бленд с деликатными пряными нотами.',
        variants: [
            ['Возможно, это HEETS PURPLE WAVE', 0],
            ['Я знаю, это HEETS CREATIONS APRICITY', 0],
            ['Конечно, это HEETS TURQUOISE SELECTION', 1],
        ],
    },



    {
        name: 'Green Zing',
        title: 'Цитрусовый <br>и свежий',
        help: 'У этого вкуса ароматические ноты «Цитрусовые»',
        image: '9',
        image2: '11',
        text: 'Табачный бленд с освежающим ментолом, усиленный цитрусовымии травяными нотами.',
        variants: [
            ['Возможно, это HEETS RUBY FUSE', 0],
            ['Я знаю, это HEETS AMARELO FUSE', 0],
            ['Конечно, это HEETS GREEN ZING', 1],
        ],

    },

    {
        name: 'Purple Wave',
        title: 'Ягодный <br>и свежий',
        help: 'У этого вкуса ароматические ноты «Ягодные»',
        image: '10',
        image2: '12',
        text: 'Табачный бленд с освежающим ментолом, раскрывающийся ароматом лесных ягод.',
        variants: [
            ['Возможно, это HEETS CREATIONS YUGEN', 0],
            ['Я знаю, это HEETS SLATE SELECTION', 0],
            ['Конечно, это HEETS PURPLE WAVE', 1],
        ],
    },

    {
        name: 'Summer Breeze',
        title: 'Кремовый <br>и свежий',
        help: 'У этого вкуса ароматические ноты «Тёплые фруктовые»',
        image: '11',
        image2: '13',
        text: 'Табачный бленд с освежающим ментолом, дополненный выразительными нотами фруктов и лёгким цветочным оттенком.',
        variants: [
            ['Я знаю, это HEETS CREATIONS APRICITY', 0],
            ['Конечно, это HEETS SUMMER BREEZE', 1],
            ['Возможно, это HEETS SATIN FUSE', 0],
        ],
    },

    {
        name: 'Creations Apricity',
        title: 'Насыщенный <br>и кремовый',
        help: 'У этого вкуса ароматические ноты «Тёплые фруктовые»',
        image: '12',
        image2: '15',
        text: 'Насыщенный табачный бленд с древесными и фруктовыми нотамии бархатистым сливочным послевкусием.',
        variants: [
            ['Конечно, это HEETS CREATIONS APRICITY', 1],
            ['Возможно, это HEETS AMBER SELECTION', 0],
            ['Я знаю, это HEETS SATIN FUSE', 0],
        ],
    },

    {
        name: 'Creations Yugen',
        title: 'Насыщенный <br>и цветочный',
        help: 'У этого вкуса ароматические ноты «Свежие цветочные»',
        image: '13',
        image2: '17',
        text: 'Изысканный табачный бленд, дополненный комбинацией фруктовых нот и цветочных ароматов с завершающим свежим аккордом.',
        variants: [
            ['Я знаю, это HEETS PURPLE WAVE', 0],
            ['Возможно, это HEETS TURQUOISE SELECTION', 0],
            ['Конечно, это HEETS CREATIONS YUGEN', 1],
        ],
    },
];
